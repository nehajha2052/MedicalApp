import React, { Component } from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import "bootstrap/dist/css/bootstrap.min.css";

class DatePick extends Component {
  state = { startDate: new Date() };

  handleChange(date) {
    this.setState({
      startDate: date,
      endDate: date,
    });
  }
  render() {
    return (
      <div>
        <form>
          <div className="form-group">
            <DatePicker
              selected={this.state.startDate}
              onChange={this.handleChange}
              showTimeSelect
              timeFormat="HH:mm"
              timeIntervals={20}
              timeCaption="time"
              dateFormat="MMM d, yyyy h:mm aa"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default DatePick;
