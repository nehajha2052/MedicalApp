import React, { Component } from "react";

export default class MedicineDetails extends Component {
  render() {
    return (
      <div>
        This page provide details about available medicines in the hospital's
        pharmacy.
      </div>
    );
  }
}
