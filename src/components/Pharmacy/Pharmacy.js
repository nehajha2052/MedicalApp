import React, { Component } from 'react';
import Title from "../Title";
import ProductList from './ProductList';

class Pharmacy extends Component {
    state = {  }
    render() { 
        return (
        <div>
            <Title name="Available" title="Medicines" />
            <ProductList />
        </div>
        );
    }
}
 
export default Pharmacy;