import React, { Component } from 'react';
import { ButtonContainer } from "../Button";
import { Link } from "react-router-dom";

class FloorWise extends Component {
    state = {  }
    render() { 
        return (
            <div>
              <div class="grid-container">
                <div>
                  <Link to="/firstfloor">
                    <ButtonContainer className="button-tiles">
                      First Floor
                    </ButtonContainer>
                  </Link>
                </div>
      
                <div>
                  <Link to="/secondfloor">
                    <ButtonContainer className="button-tiles">
                      Second Floor
                    </ButtonContainer>
                  </Link>
                </div>
      
                <div>
                  <Link to="/thirdfloor">
                    <ButtonContainer className="button-tiles">
                      Third Floor
                    </ButtonContainer>
                  </Link>
                </div> 
              </div>
            </div>
          );
    }
}
 
export default FloorWise;