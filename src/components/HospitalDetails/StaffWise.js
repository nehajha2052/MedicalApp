import React, { Component } from 'react';
import AutoCompleteText from "../AutoCompleteText";
import staffs from '../Data/staffs';

class StaffWise extends Component {
    state = {  }
    render() {        
            return (<div className="App-Component">
                <label>Enter Staff's Name:</label>
              <AutoCompleteText items={staffs} />
            </div>
            );        
    }
}
 
export default StaffWise;