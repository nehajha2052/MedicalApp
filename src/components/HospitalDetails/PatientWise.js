import React, { Component } from 'react';
import AutoCompleteText from "../AutoCompleteText";
import patients from "../Data/patients";

class PatientWise extends Component {
    state = {  }
    render() { 
        return (<div className="App-Component">
            <label>Enter Patient Name:</label>
          <AutoCompleteText items={patients} />
        </div>);
    }
}
 
export default PatientWise;