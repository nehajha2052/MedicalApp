import React, { Component } from 'react';
import { ButtonContainer } from "../Button";
import { Link } from "react-router-dom";

class DepartmentWise extends Component {
    state = {  }
    render() { 
        return (
            <div>
              <div class="grid-container">
                <div>
                  <Link to="/skin">
                    <ButtonContainer className="button-tiles">
                      Skin
                    </ButtonContainer>
                  </Link>
                </div>
      
                <div>
                  <Link to="/bone">
                    <ButtonContainer className="button-tiles">
                      Bone
                    </ButtonContainer>
                  </Link>
                </div>
      
                <div>
                  <Link to="/emergency">
                    <ButtonContainer className="button-tiles">
                      Emergency
                    </ButtonContainer>
                  </Link>
                </div> 

                <div>
                  <Link to="/heart">
                    <ButtonContainer className="button-tiles">
                      Heart
                    </ButtonContainer>
                  </Link>
                </div> 

                <div>
                  <Link to="/general">
                    <ButtonContainer className="button-tiles">
                      General
                    </ButtonContainer>
                  </Link>
                </div> 
              </div>
            </div>
          );
    }
}
 
export default DepartmentWise;