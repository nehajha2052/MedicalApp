import React, { useState } from "react";
import Note from "./Note";
// import notes from "../notes";
import CreateArea from "./CreateArea";
import "./NoteTaking.css";

function NoteTaking() {
    // react useState ...
    const [notes, setNotes] = useState([]);

    function addNote(newNote) {
        setNotes((prevNotes) => {
          // using spread operator
          return [...prevNotes, newNote];
        });
      }
    
      function deleteNote(id) {
        setNotes((prevNotes) => {
          return prevNotes.filter((noteItem, index) => {
            return index !== id;
          });
        });
      }
    
      return (
        <div>
             <CreateArea onAdd={addNote} />
      {notes.map((noteItem, index) => {
        return (
          <Note
            key={index}
            id={index}
            title={noteItem.title}
            content={noteItem.content}
            onDelete={deleteNote}
          />
        );
      })}
      </div>
  );
}

export default NoteTaking;