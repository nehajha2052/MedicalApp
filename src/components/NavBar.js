import React, { Component } from "react";
import logo from "../logo.svg";
import { Link } from "react-router-dom";
import { ButtonContainer } from "./Button";
import styled from "styled-components";

export default class NavBar extends Component {
  render() {
    return (
      <div>
        <NavWrapper className="navbar navbar-expand-sm navbar-dark px-sm-5">
          {/* https://www.iconfinder.com/icons/1243689/call_phone_icon Creative
          Commons (Attribution 3.0 Unported);
          https://www.iconfinder.com/Makoto_msk */}
          <Link to="/">
            <img src={logo} alt="store" className="navbar-brand"></img>
          </Link>
          <ul className="navbar-nav align-items-center">
            <li className="nav-item ml-5">
              <Link to="/" className="nav-link">
                MediCity
              </Link>
            </li>
          </ul>
          <ul className="navbar-nav align-items-center">
            <li className="nav-item ml-5">
              <Link to="/dashboard" className="nav-link">
                Chat
              </Link>
            </li>
          </ul>
          <ul className="navbar-nav align-items-center">
            <li className="nav-item ml-5">
              <Link to="/root" className="nav-link">
                Root
              </Link>
            </li>
          </ul>
          <Link to="/login" className="ml-auto">
            <ButtonContainer>Log In</ButtonContainer>
          </Link>
        </NavWrapper>
      </div>
    );
  }
}

const NavWrapper = styled.nav`
  background: var(--mainBlue);
  .nav-link {
    color: var(--mainWhite) !important;
    font-size: 1.3rem;
    text-transform: capitalize;
  }
`;
