import React, { Component } from "react";
import AutoCompleteText from "./AutoCompleteText";
import patients from "./Data/patients";

export default class AppointmentDetails extends Component {
  render() {
    return (<div className="App-Component">
                <label>Enter Patient's Name:</label>
              <AutoCompleteText items={patients} />
            </div>
            );  
  }
}
