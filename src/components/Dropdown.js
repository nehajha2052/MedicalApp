import React, { Component } from "react";

class DropDown extends Component {
  state = {};
  render() {
    return (
      <div>
        <select id="doctor" name="doctor">
          <option value="rabindrathakur">Dr. Rabindra Thakur</option>
          <option value="kamalasharma">Dr. Kamala Sharma</option>
          <option value="shafaljha">Dr. Shafal Jha</option>
          <option value="sabitakc">Dr. Sabita K.C.</option>
          <option value="manishshrestha">Dr. Manish Shrestha</option>
        </select>
      </div>
    );
  }
}

export default DropDown;
