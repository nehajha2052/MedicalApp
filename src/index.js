import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import {ProductProvider} from "./components/Pharmacy/context";
import firebase from '@firebase/app';

require("firebase/firestore");

var firebaseConfig = {
  apiKey: "AIzaSyDk2l7GZ72VPaWMTsxOCWei3_olSQDWPw8",
  authDomain: "medical-app-c3f13.firebaseapp.com",
  projectId: "medical-app-c3f13",
  storageBucket: "medical-app-c3f13.appspot.com",
  messagingSenderId: "163726651273",
  appId: "1:163726651273:web:d838d185c5ac36a5977b58",
  measurementId: "G-1FDNGNPRYN"
};
firebase.default.initializeApp(firebaseConfig);
firebase.analytics();

ReactDOM.render(
  <ProductProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </ProductProvider>,
  document.getElementById("root")
);


